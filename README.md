# Desafio Labi9 Frontend - React


## Criar um painel admin com autenticação e rotas protegidas

Desafio técnico criado para medir seus conhecimentos e habilidades de programação frontend (React).

A maior parte de tudo que é pedido neste desafio pode ser encontrado na documentação do [React](https://react.dev/learn)

Para as requisições utilizar a seguinte [API](https://challenge-labi9-4b4c472d5c07.herokuapp.com/api/docs)


## Telas


Não autenticado:

- Registrar

- Login

Autenticado:


- Listar catetgorias

- Cadastrar catetgoria

- Listar produtos

- Cadastrar produto

- Alterar produto

- Visualizar um produto

Layout de exemplo para a criação das telas (Apenas se basear neste exemplo) [link](https://demos.creative-tim.com/vue-notus/?AFFILIATE=52980#/admin/tables)


## O que será avaliado:

- Qualidade e simplicidade do código.

- Utilização dos padrões Ecma e React em sua ultima versão.

- Utilização dos recursos do ecosistema React (sem next).

- Autenticação e rotas protegidas.

- Css da pagina

- Utilização do Typescript

- Componentização


## Diferencial

- Responsividade

- Tests cobrindo todos os pontos

- Utilização do inglês no codigo

- Utilização do tailwind


## Aplicação

A aplicação é basicamente um painel admin onde o usuario poderá se registrar e se logar.
Após estar logado ele tem acesso as seguintes funcionalidades:

- Cadastrar categoria;
- Listar categorias;
- Cadastrar produto;
- Visualizar produto;
- Listar produtos;
- Alterar produto;
- Deletar produto;
- Pesquisar produtos pelo nome;
- Deslogar da aplicação.

## Entrega

A aplicação deve ser criada em modo privado no seu repositorio github ou gitlab,
ao termino será necessario enviar um email para comercial@labi9.com e adicionar @GreatGui (caso utilizar github  adicionar [GreatGui](https://github.com/GreatGui)) ao repositorio.
